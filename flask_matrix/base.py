class FlaskMatrix(object):
    def __init__(self, app=None):
        self.api = None
        self.app = None

        if app:
            self.init_app(app)

    def init_app(self, app):
        app.config.setdefault('MATRIX_BASE_URL', None)
        app.config.setdefault('MATRIX_USERNAME', None)
        app.config.setdefault('MATRIX_PASSWORD', None)
        app.config.setdefault('MATRIX_TOKEN', None)

        if app.config['MATRIX_USERNAME'] is None and app.config['MATRIX_PASSWORD'] is None \
           app.config['MATRIX_TOKEN'] is None:
            raise ValueError('You must specify at least MATRIX_TOKEN or both MATRIX_USERNAME and MATRIX_PASSWORD')

        if app.config['MATRIX_BASE_URL'] is None:
            raise ValueError('MATRD]IX_BASE_URL is not set!')

        self.username = app.config['MATRIX_USERNAME']
        self.password = app.config['MATRIX_PASSWORD']

        self.client = MatrixHttpApi(app.config['MATRIX_BASE_URL'], token=app.config['MATRIX_TOKEN'])
